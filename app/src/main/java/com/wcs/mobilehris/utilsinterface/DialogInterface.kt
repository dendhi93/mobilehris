package com.wcs.mobilehris.utilsinterface

interface DialogInterface {
    fun onPositiveClick (o : Any)
    fun onNegativeClick (o: Any)
}
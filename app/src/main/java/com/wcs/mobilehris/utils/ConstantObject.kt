package com.wcs.mobilehris.utils

object ConstantObject {
    //toast
    const val vToastSuccess = 1
    const val vToastError = 2
    const val vToastInfo = 3

    //snack bar
    const val vSnackBarWithButton = 1
    const val vSnackBarNoButton = 2

    //alertDialog
    const val vAlertDialogDismiss = 1
    const val vAlertDialogOkCancel = 2
    const val vAlertDialogNoConnection = "No Connection"

    //UI type
    const val vButtonUI = 1
    const val vProgresBarUI = 2

}